$(document).ready(function () {
  console.log('Ready!');

  // Scroll Events
  $(window).scroll(function () {

    var wScroll = $(this).scrollTop();

    // Activate menu
    if (wScroll > 20) {
      $('#main-nav').addClass('active');
      $('#slide_out_menu').addClass('scrolled');
    }
    else {
      $('#main-nav').removeClass('active');
      $('#slide_out_menu').removeClass('scrolled');
    }
  });

  // Navigation
  $('#navigation-btn').on('click', function(e){
    e.preventDefault();
    $(this).addClass('open');
    $('#slide-out-menu').toggleClass('open');

    if ($('#slide-out-menu').hasClass('open')) {
      $('.close-menu').on('click', function(e){
        e.preventDefault();
        $('#slide-out-menu').removeClass('open');
      })
    }
  });

  // Wow Animations
  wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  );
  wow.init();

});